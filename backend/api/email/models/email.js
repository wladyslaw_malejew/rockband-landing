'use strict';

/**
 * Lifecycle callbacks for the `email` model.
 */

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model, attrs, options) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  afterSave: async (model, response, options) => {
    let usersData, userName, mailList;

    usersData = await strapi.plugins['users-permissions'].services.user.fetchAll()

    mailList = usersData.map(ud => ud.email)
    // userName = usersData.map(ud => ud.username)

    console.log(mailList);

    await strapi.plugins['email'].services.email.send({
      to: mailList,
      // from: 'rockband@mail.com',
      subject: `${response.subject}`,
      text: `${response.text}`,
    });
  },

  //   const sgMail = require('@sendgrid/mail');

  //   sgMail.setApiKey('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');

  //   const msg = {
  //     to: 'vm23lm10@gmail.com',
  //     from: 'test@example.com',
  //     subject: 'Sending with Twilio SendGrid is Fun',
  //     text: 'and easy to do anywhere, even with Node.js',
  //     html: '<strong>and easy to do anywhere, even with Node.js</strong>',
  //   };

  //   sgMail.send(msg);

  // }

  // Before fetching a value.
  // Fired before a `fetch` operation.
  // beforeFetch: async (model, columns, options) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, response, options) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model, columns, options) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, response, options) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model, attrs, options) => {},

  // After creating a value.
  // Fired after an `insert` query.
  // afterCreate: async (model, attrs, options) => {},

  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model, attrs, options) => {},

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, attrs, options) => {},

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model, attrs, options) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, attrs, options) => {}
};
