module.exports = {
  apps: [
    {
      name: 'strapi',
      cwd: '/root/projects/rockband-landing/backend',
      script: 'npm',
      args: 'start',
      env: {
	  HOST: 'vladmaleev.online',
        NODE_ENV: 'production',
        DATABASE_HOST: '', // database endpoint
        DATABASE_PORT: '',
        DATABASE_NAME: 'rockband', // DB name
        DATABASE_USERNAME: '', // your username for psql
        DATABASE_PASSWORD: '', // your password for psql
      },
    },
  ],
};